package com.example.pietrogirardi.eventbus.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.example.pietrogirardi.eventbus.R;
import com.example.pietrogirardi.eventbus.eventbus.MessageEB;

import de.greenrobot.event.EventBus;


public class FragmentTop extends Fragment implements View.OnClickListener {

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                            @Nullable Bundle savedInstanceState) {

        Log.i("LOG", "FragmentTop - onCreateView() 5");

        View view = inflater.inflate(R.layout.fragment_top, null);


        Button btDataFragmentBottom = (Button) view.findViewById(R.id.btDataFragmentBottom);
        btDataFragmentBottom.setOnClickListener(FragmentTop.this);

        // EventBus register
        EventBus.getDefault().register(FragmentTop.this);

        return view;
    }

    @Override
    public void onClick(View v) {
        Log.i("LOG", "FragmentTop - btnFragmentBottom click()");
        MessageEB m = new MessageEB();
        m.setText("message came from FragmentBottom");
        m.setClassTester(FragmentBottom.class + "");

        EventBus.getDefault().post(m);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        // EventBus Unregister
        EventBus.getDefault().unregister(FragmentTop.this);
        Log.i("LOG", "FragmentTop - onDestroy()");
    }


    public void onEvent(MessageEB messageEB){
        Log.i("LOG", "FragmentTop - onEvent()");
        if(!messageEB.getClassTester().equals(FragmentTop.class+""))
            return;

        Toast.makeText(getActivity(), messageEB.getText(), Toast.LENGTH_LONG).show();

    }
}
