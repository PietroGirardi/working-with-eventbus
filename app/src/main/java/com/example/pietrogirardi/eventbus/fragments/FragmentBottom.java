package com.example.pietrogirardi.eventbus.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.example.pietrogirardi.eventbus.R;
import com.example.pietrogirardi.eventbus.eventbus.MessageEB;

import de.greenrobot.event.EventBus;


/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentBottom extends Fragment implements View.OnClickListener{


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        Log.i("LOG", "FragmentBottom - onCreateView() 6");

        View view = inflater.inflate(R.layout.fragment_bottom, container, false);

        Button btDataFragmentTop = (Button) view.findViewById(R.id.btDataFragmentTop);
        btDataFragmentTop.setOnClickListener(FragmentBottom.this);

        // EventBus register
        EventBus.getDefault().register(FragmentBottom.this);

        return view;
    }


    @Override
    public void onClick(View v) {
        Log.i("LOG", "FragmentBottom - onClick()");
        MessageEB m = new MessageEB();
        m.setText("message came from FragmentTop");
        m.setClassTester(FragmentTop.class + "");

        EventBus.getDefault().post(m);
    }

    @Override
    public void onDestroy() {
        Log.i("LOG", "FragmentBottom - onDestroy()");
        super.onDestroy();

        // EventBus Unregister
        EventBus.getDefault().unregister(FragmentBottom.this);
    }

    public void onEvent(MessageEB messageEB){
        Log.i("LOG", "FragmentBottom - onEvent()");
        if(!messageEB.getClassTester().equals(FragmentBottom.class+""))
            return;

        Toast.makeText(getActivity(), messageEB.getText(), Toast.LENGTH_LONG).show();

    }
}
