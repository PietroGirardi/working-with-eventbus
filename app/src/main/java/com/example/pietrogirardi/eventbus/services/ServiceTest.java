package com.example.pietrogirardi.eventbus.services;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

import com.example.pietrogirardi.eventbus.MainActivity;
import com.example.pietrogirardi.eventbus.eventbus.MessageEB;
import com.example.pietrogirardi.eventbus.models.Person;

import java.util.ArrayList;
import java.util.List;

import de.greenrobot.event.EventBus;

/**
 * Created by pietrogirardi on 02/11/15.
 */
public class ServiceTest extends Service {
    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        Log.i("LOG", "Service - onCreate() 2");

        // EventBus Register
        EventBus.getDefault().register(ServiceTest.this);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        Log.i("LOG", "Service - onStartCommand() 3");
        countThread();

        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        // EventBus Unregister
        EventBus.getDefault().unregister(ServiceTest.this);

        Log.i("LOG", "Service - onDestroy()");
    }



    public void countThread(){
        Thread t = new Thread(new Runnable(){
            public void run(){
                for(int i = 0; i < 2; i++){
                    Log.i("LOG", "Service - countThread() 4 num = " + i);

                    MessageEB m = new MessageEB();
                    m.setClassTester(MainActivity.class + "");
                    m.setNumber(i + 1);
                    m.setText("Random message: " + (i + 1));

                    EventBus.getDefault().post(m);

                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
        t.start();
    }

    public void onEvent(MessageEB mMessageEB){
        Log.i("LOG", "Service - onEvent()");

        if(!mMessageEB.getClassTester().equalsIgnoreCase(ServiceTest.class+""))
            return;

        Person p = new Person("Pietro", "Developer");
        List<Person> list = new ArrayList<Person>();
        list.add(p);
        mMessageEB.setClassTester(MainActivity.class + "");
        mMessageEB.setList(list);

        EventBus.getDefault().postSticky(mMessageEB);
        Log.i("LOG", "Service - postSticky()");
    }

}
